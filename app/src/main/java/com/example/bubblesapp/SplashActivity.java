package com.example.bubblesapp;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

public class SplashActivity extends AppCompatActivity {

    public MediaPlayer splashSound;
    private static final int SPLASH_DISPLAY_TIME = 3000;
    public Vibrator vibrator;

    public static final int STARTUP_DELAY = 500;
    public static final int ANIM_ITEM_DURATION = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        splashSound = MediaPlayer.create(SplashActivity.this, R.raw.bubbles_sound);
        ImageView logo = findViewById(R.id.logo);

        splashSound.setOnCompletionListener(new
            MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer splashSound) {

                    splashSound.stop();
                    splashSound.release();

                }
            });
        splashSound.start();

        Animation fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        logo.startAnimation(fadeInAnimation);

        ViewCompat.animate(logo)
                .translationY(-500)
                .setStartDelay(STARTUP_DELAY)
                .setDuration(ANIM_ITEM_DURATION).setInterpolator(
                new DecelerateInterpolator(1.2f)).start();

        new Handler().postDelayed(new Runnable() {
            public void run() {

                Intent intent = new Intent();
                intent.setClass(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                vibrator.vibrate(1000);
                finish();

            }
        }, SPLASH_DISPLAY_TIME);
    }
}